package com.voroj.opencart;

import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;

import static com.voroj.opencart.Constants.DOLLAR;
import static com.voroj.opencart.Constants.EURO;
import static com.voroj.opencart.Constants.RUB;
import static com.voroj.opencart.Constants.RUS_LANGUAGE;
import static com.voroj.opencart.Constants.STERLING;
import static com.voroj.opencart.Constants.TEL_NUMBER_SITE;
import static com.voroj.opencart.Constants.TITLE_SITE;
import static com.voroj.opencart.Constants.URL_SITE;
import static com.voroj.opencart.Constants.USA_LANGUAGE;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Owner("Vorojeykin Dmitry")
@Epic("Smoke test")
public class MainPageTest extends BaseTest {

    @Severity(value = SeverityLevel.NORMAL)
    @Test(description = "Проверить изменение валюты на Рубль")
    public void checkSelectRusCurrentTest() {
        MainPage mainPage = new MainPage();
        mainPage.setRub();
        assertEquals(mainPage.getTextCurrent(), RUB);
    }

    @Severity(value = SeverityLevel.NORMAL)
    @Test(description = "Проверить изменение валюты на Евро")
    public void checkSelectEurCurrentTest() {
        MainPage mainPage = new MainPage();
        mainPage.setEuro();
        assertEquals(mainPage.getTextCurrent(), EURO);
    }

    @Severity(value = SeverityLevel.NORMAL)
    @Test(description = "Проверить изменение валюты на Доллар")
    public void checkSelectUSDCurrentTest() {
        MainPage mainPage = new MainPage();
        mainPage.setDollar();
        assertEquals(mainPage.getTextCurrent(), DOLLAR);
    }

    @Severity(value = SeverityLevel.NORMAL)
    @Test(description = "Проверить изменение валюты на Стерлинг")
    public void checkSelectFunCurrentTest() {
        MainPage mainPage = new MainPage();
        mainPage.setSterling();
        assertEquals(mainPage.getTextCurrent(), STERLING);
    }

    @Severity(value = SeverityLevel.NORMAL)
    @Test(description = "Проверить номер телефона сайта")
    public void checkNumberTelephoneSiteTest() {
        MainPage mainPage = new MainPage();
        assertTrue(mainPage.checkNumber().contains(TEL_NUMBER_SITE));
    }

    @Severity(value = SeverityLevel.MINOR)
    @Test(description = "Проверить изменение языка сайта на английский")
    public void checkSelectEnglishLanguageTest() {
        MainPage mainPage = new MainPage();
        mainPage.setSelectEnglish();
        assertEquals(mainPage.getTexAllLanguage(), USA_LANGUAGE);
    }

    @Severity(value = SeverityLevel.CRITICAL)
    @Test(description = "Проверить изменение языка сайта на русский")
    public void checkSelectRussianLangTest() {
        MainPage mainPage = new MainPage();
        mainPage.setSelectRussian();
        assertEquals(mainPage.getTexAllLanguage(), RUS_LANGUAGE);
    }

    @Severity(value = SeverityLevel.CRITICAL)
    @Test(description = "Проверить название сайта и адрес")
    public void verifyUrlTest() {
        MainPage mainPage = new MainPage();
        String url = driver.getCurrentUrl();
        String title = driver.getTitle();
        assertEquals(url, URL_SITE);
        assertEquals(title, TITLE_SITE);
    }

}
