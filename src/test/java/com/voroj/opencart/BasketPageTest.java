package com.voroj.opencart;

import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;


import static com.voroj.opencart.Constants.CERTIFICATE;
import static com.voroj.opencart.Constants.COUPON;
import static com.voroj.opencart.Constants.EMAIL;
import static com.voroj.opencart.Constants.INDEX_POST;
import static com.voroj.opencart.Constants.PASSWORD;
import static com.voroj.opencart.Constants.USE_SER_TEXT;
import static org.testng.Assert.assertTrue;

@Owner("Vorojeykin Dmitry")
@Epic("Smoke test")
public class BasketPageTest extends BaseTest {

    @Severity(value = SeverityLevel.TRIVIAL)
    @Test(description = "Проверить изменение количества товара в корзине")
    public void updateCountOrderTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        BasketPage basket = new BasketPage();
        basket.checkOrderInBasket(USE_SER_TEXT);
        assertTrue(basket.getTextCheckModelOrder().contains(USE_SER_TEXT));
    }

    @Severity(value = SeverityLevel.CRITICAL)
    @Test(description = "Проверить добавление купона на скидку", dependsOnMethods = {"updateCountOrderTest"})
    public void addCouponTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        mainPage.transitionInBasket()
                .addCoupon(COUPON);
        BasketPage basket = new BasketPage();
        assertTrue(basket.checkElDisplayedMethod());
    }

    @Severity(value = SeverityLevel.CRITICAL)
    @Test(description = "Проверить расчет стоимости доставки", dependsOnMethods = {"updateCountOrderTest"})
    public void calcPriceDeliveryInBasketTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        mainPage.transitionInBasket()
                .calkPriceDelivery(INDEX_POST);
        BasketPage basket = new BasketPage();
        assertTrue(basket.checkElDisplayedMethod());
    }

    @Severity(value = SeverityLevel.CRITICAL)
    @Test(description = "Проверить ввод подарочного сертификата", dependsOnMethods = {"updateCountOrderTest"})
    public void useGiftCertificateTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        mainPage.transitionInBasket()
                .useGiftCertificateMethod(CERTIFICATE);
        BasketPage basket = new BasketPage();
        assertTrue(basket.checkElDisplayedMethod());

    }

}
