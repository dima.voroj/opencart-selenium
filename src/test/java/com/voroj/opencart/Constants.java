package com.voroj.opencart;

public class Constants {

    /**
     * переменные класса MainPageTest
     */
    public static final String RUS_LANGUAGE = "Язык";
    public static final String USA_LANGUAGE = "Language";
    public static final String EURO = "€";
    public static final String STERLING = "£";
    public static final String DOLLAR = "$";
    public static final String RUB = "руб.";
    public static final String URL_SITE = "https://demo-opencart.ru/";
    public static final String TITLE_SITE = "Your Store";
    public static final String TEL_NUMBER_SITE = "123456789";
    /**
     * переменные класса LoginPageTest
     */
    public static final String EMAIL = "dima.vo@yandex.ru";
    public static final String PASSWORD = "12345";
    public static final String TEXT_LK = "Моя учетная запись";
    /**
     * переменные класса PaymentInformationTest
     */

    /**
     * переменные класса RegistrationTest
     */
    public static final String NAME = "Dimaaa";
    public static final String LASTNAME = "Vorojjj";
    public static final String EMAIL_REG = "dimaaa.vo@yandex.ru";
    public static final String TELEPHONE = "89052652052";
    public static final String YES = "Поздравляем! Ваш Личный Кабинет был успешно создан.";
    public static final String NOT = "Если Вы уже зарегистрированы, перейдите на страницу авторизации.";
    public static final String INVALID_NAME = "$Dima!!!$%&";
    public static final String INVALID_LASTNAME = "*)";
    public static final String INVALID_EMAIL = "dimaaa.yandex.ru";
    public static final String INVALID_TELEPHONE = "1234567890qwert";
    public static final String INVALID_PASSWORD = "1a2";
    public static final String EMPTY = "";
    /**
     * переменные класса PersonalAriaTest
     */
    public static final String MODEL_PRODUCT = "Product 21";
    public static final String RETURN_MESSAGE = "Вы отправили запрос на возврат.";
    public static final String MESSAGE = "Мне не понравилась упаковка товара";
    public static final String TEXT_HISTORY_TRANSACTION = "Ваши транзакции";
    public static final String DOWNLOADS_FILE = "Файлы для скачивания";
    public static final String LOGOUT_MESSAGE = "Вы вышли из Личного Кабинета.";
    /**
     * переменные класса BookmarksTest
     */
    public static final String TEXT_CONFIRM_ORDER = " Вы добавили ";
    public static final String COUNT_BOOKMARKS = "Закладки (1)";
    public static final String TEXT_CLEAR_BOOKMARKS = "Ваша корзина пуста!";
    /**
     * константы класса BasketTest
     */
    public static final String CHECK_COUNT_ORDER = "1";
    public static final String USE_SER_TEXT = "Использовать Подарочный сертификат";
    public static final String COUPON = "123456";
    public static final String INDEX_POST = "12345";
    public static final String ERROR_COUPON = "Ошибка. Неправильный код Купона на скидку. Возможно," +
            " истек срок действия или достигнут лимит использования!";
    public static final String PRICE_DELIVERY = "5.00руб.";
    public static final String CERTIFICATE = "123456";
    public static final String CALCULATE_DELIVERY = "Расчет успешно выполнен!";
    /**
     * константы класса MakingAnOrder
     */
    public static final String NAME_VLADIMIR = "Владимир";
    public static final String LAST_NAME_VLAD = "Владимирович";
    public static final String COMPANY = "Google";
    public static final String ADDRESS = "Российская Федерация";
    public static final String ADDRESS2 = "Ленинградская область";
    public static final String CITY = "Санкт-Петербург";
    public static final String POST_CODE_ST_P = "33333";
    public static final String CHECK_CONTAINS = "Владимир Владимирович 33333";
    public static final String MARK_PRODUCT = "Notebook";
    public static final String DATE = "2022-07-30";
    public static final String COUNT_ORDER = "1";
    public static final String ORDER_CONFIRM = "Ваш заказ принят!";
    public static final String NOTEBOOK = "HP LP3065";
    public static final String COMMENT = "У вас лучший магазин";
    public static final String SELECT_VALUE = "303";
    /**
     * константы класса SupportService
     */
    public static final String CHECK_ADDRESS = "Address 1";
    public static final String CHECK_ADDRESS2 = "Компьютеры Алтуфьево";
    public static final String CONTACTS = "Контакты";
    public static final String MESSAGE_FIT_BACK = "Верните мне деньги, спасибо!";
    public static final String NUMBER_ORDER = "333";
    public static final String MESSAGE_RETURN = "Вы отправили запрос на возврат.";
    public static final String MAP_SITE_TEXT = "Карта сайта";
    /**
     * константы класса Additionally
     */
    public static final String APPLE = "Apple";
    public static final String CANON = "Canon";
    public static final String HEWLETT_PACKARD = "Hewlett-Packard";
    public static final String HTC = "HTC";
    public static final String PALM = "Palm";
    public static final String SONY = "Sony";
    public static final String YOU_NAME = "Egor";
    public static final String GIFT_MESSAGE = "С праздником!";
    public static final String AMOUNT = "555";
    public static final String THANK = "Спасибо за покупку подарочного сертификата!";
    public static final String NAME_PATRONUMIG = "Дмитрий, Юрьевич";
    public static final String SURNAME = "Ворожейкин";
    public static final String NEW_EMAIL = "diman.rus@yandex.ru";
    public static final String NEW_TELEPHONE = "89373812349";
    public static final String NEW_COMPANY = "QA automation";
    public static final String WEB_SITE = "https://www.google.com/";
    public static final String INN = "123456789";
    public static final String NEW_PASSWORD = "55555";
    public static final String TEXT_NEW_PARTNER_ACC = "Поздравляем. Ваш партнерский аккаунт успешно создан!";
    public static final String STOCK = "Акции";
}