package com.voroj.opencart;

import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;

import static com.voroj.opencart.Constants.*;

import static org.testng.Assert.assertTrue;

@Owner("Vorojeykin Dmitry")
@Epic("Smoke test")
public class PurchasesPageTest extends BaseTest {

    @Severity(value = SeverityLevel.CRITICAL)
    @Test(description = "Проверить добавление товара в корзину без авторизации")
    public void addOrderInBasketNotRegistrationTest() {
        MainPage mainPage = new MainPage();
        mainPage.transitionAnPurchases()
                .showAddNotebookInBasket(DATE, COUNT_ORDER);
        PurchasesPage purchasesPage = new PurchasesPage();
        assertTrue(purchasesPage.checkAddOrderInBasket().contains(NOTEBOOK));
    }

    @Severity(value = SeverityLevel.BLOCKER)
    @Test(description = "Проверить добавление товара в корзину авторизованного пользователя")
    public void addOrderInBasketTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        mainPage.transitionAnPurchases()
                .showAddNotebookInBasket(DATE, COUNT_ORDER);
        PurchasesPage purchasesPage = new PurchasesPage();
        assertTrue(purchasesPage.checkAddOrderInBasket().contains(NOTEBOOK));
    }

}
