package com.voroj.opencart;

import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;

import static com.voroj.opencart.Constants.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

@Owner("Vorojeykin Dmitry")
@Epic("Smoke test")
public class AdditionallyPageTest extends BaseTest {

    @Severity(value = SeverityLevel.MINOR)
    @Test(description = "Проверить отображение производителей техники")
    public void checkManufacturesTest() {
        MainPage mainPage = new MainPage();
        mainPage.transitionInManufacturers();
        AdditionallyPage additionally = new AdditionallyPage();
        assertEquals(additionally.checkApple(), APPLE);
        assertEquals(additionally.checkCanon(), CANON);
        assertEquals(additionally.checkHewlett(), HEWLETT_PACKARD);
        assertEquals(additionally.checkHtc(), HTC);
        assertEquals(additionally.checkSony(), SONY);
    }

    @Severity(value = SeverityLevel.BLOCKER)
    @Test(description = "Проверить отправку подарочного сертификата")
    public void giftVoucherSendingVerificationTest() {
        MainPage mainPage = new MainPage();
        mainPage.transitionInGiftCertificate()
                .inputData(NAME, EMAIL_REG, YOU_NAME, EMAIL, GIFT_MESSAGE, AMOUNT);
        AdditionallyPage additionally = new AdditionallyPage();
        assertTrue(additionally.getTextMessageThank().contains(THANK));
    }

    @Severity(value = SeverityLevel.BLOCKER)
    @Test(description = "Проверить регистрацию партнерского аккаунта")
    public void registrationPartnerAccountTest() {
        MainPage mainPage = new MainPage();
        mainPage.transitionInRegistrationNewPartner()
                .registrationPartnerAcc(NAME_PATRONUMIG, SURNAME, NEW_EMAIL, NEW_TELEPHONE, COMPANY,
                                         WEB_SITE, INN, NEW_PASSWORD, NEW_PASSWORD);
        AdditionallyPage additionally = new AdditionallyPage();
        assertFalse(additionally.getTextPartnerAcc().contains(TEXT_NEW_PARTNER_ACC));
    }

    @Severity(value = SeverityLevel.BLOCKER)
    @Test(description = "Выполнить вход в партнерский аккаунт")
    public void signInPartnerAcc() {
        MainPage mainPage = new MainPage();
        mainPage.transitionInRegistrationNewPartner()
                .signIn(NEW_EMAIL,NEW_PASSWORD);
        AdditionallyPage additionally = new AdditionallyPage();
        assertEquals(additionally.getTextLk(), TEXT_LK);
    }

    @Severity(value = SeverityLevel.NORMAL)
    @Test(description = "Проверить акции на товары сайта")
    public void checkStockTest() {
        MainPage mainPage = new MainPage();
        mainPage.transitionInStock();
        AdditionallyPage additionally = new AdditionallyPage();
        assertEquals(additionally.getTextStock(), STOCK);
    }
}
