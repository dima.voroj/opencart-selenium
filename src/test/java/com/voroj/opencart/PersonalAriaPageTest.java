package com.voroj.opencart;

import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;

import static com.voroj.opencart.Constants.*;
import static org.testng.Assert.assertEquals;

@Owner("Vorojeykin Dmitry")
@Epic("Smoke test")
public class PersonalAriaPageTest extends BaseTest {

    @Severity(value = SeverityLevel.CRITICAL)
    @Test(description = "Проверить историю заказов")
    public void checkHistoryOrderTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        PersonalAriaPage personalAria = new PersonalAriaPage();
        personalAria
                .checkHistoryOrders()
                .checkInfOrderMethod();
        assertEquals(personalAria.checkModelOrderMethod(), MODEL_PRODUCT);
    }

    @Severity(value = SeverityLevel.BLOCKER)
    @Test(description = "Проверить возврат товара")
    public void returnOrderTest() {
        MainPage mainPage = new MainPage();
        mainPage
                .lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        PersonalAriaPage personalAria = new PersonalAriaPage();
        personalAria
                .checkHistoryOrders()
                .checkInfOrderMethod()
                .returnOrderMethod()
                .returnOrderErrorMethod()
                .descriptionProblemsOrderMethod(MESSAGE)
                .submitMethod();
        assertEquals(personalAria.getTextMethod(), RETURN_MESSAGE);
    }

    @Severity(value = SeverityLevel.CRITICAL)
    @Test(description = "Проверить историю транзакций")
    public void checkHistoryTransactionTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        PersonalAriaPage personalAria = new PersonalAriaPage();
        personalAria
                .checkTransactionMethod();
        assertEquals(personalAria.getTextHistTransMethod(), TEXT_HISTORY_TRANSACTION);
    }

    @Severity(value = SeverityLevel.MINOR)
    @Test(description = "Проверить историю загрузок файлов")
    public void checkHistoryDownloadsFileTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        PersonalAriaPage personalAria = new PersonalAriaPage();
        personalAria
                .checkLoadsMethod();
        assertEquals(personalAria.getTextDownloadsFileMethod(), DOWNLOADS_FILE);
    }

    @Severity(value = SeverityLevel.CRITICAL)
    @Test(description = "Проверить выход из личного кабинета")
    public void checkLogoutTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        PersonalAriaPage personalAria = new PersonalAriaPage();
        personalAria
                .logOutMethod();
        assertEquals(personalAria.logoutTextMethod(), LOGOUT_MESSAGE);
    }

}
