package com.voroj.opencart;

import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;

import static com.voroj.opencart.Constants.*;
import static org.testng.Assert.assertEquals;

@Owner("Vorojeykin Dmitry")
@Epic("Smoke test")
public class BookmarksTest extends BaseTest {

    @Severity(value = SeverityLevel.CRITICAL)
    @Test(description = "Проверить добавление товара в закладки")
    public void addBookmarksTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        mainPage
                .transitionAnPurchases()
                .addBookmarksMet()
                .addBookMarksMethod();
        Bookmarks bookmarks = new Bookmarks();
        assertEquals(bookmarks.getTextCheckBookmarks(), COUNT_BOOKMARKS);
    }

    @Severity(value = SeverityLevel.CRITICAL)
    @Test(description = "Проверить удаление товара из закладок", dependsOnMethods = {"addBookmarksTest"})
    public void clearBookmarksTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        Bookmarks bookmarks = new Bookmarks();
        bookmarks.checkBookmarksMethod()
                .clearBookmarksMethod();
        assertEquals(bookmarks.getTextClearBookmarks(), TEXT_CLEAR_BOOKMARKS);
    }
}
