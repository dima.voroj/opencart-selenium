package com.voroj.opencart;

import io.qameta.allure.Epic;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;

import static com.voroj.opencart.Constants.*;
import static org.testng.Assert.assertEquals;

@Epic("Smoke test")
public class MakingAnOrderPageTest extends BaseTest {

    @Severity(value = SeverityLevel.BLOCKER)
    @Test(description = "Проверить оформление покупки с добавлением нового адреса доставки")
    public void addNewUserInfAndPurchasesTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        mainPage.transitionInBasket()
                .checkOrderInBasket(USE_SER_TEXT);
        mainPage.transitionInBasket()
                .transitionInMakingAnOrder()
                .useNewAddressMethod(NAME_VLADIMIR, LAST_NAME_VLAD, COMPANY,
                        ADDRESS, ADDRESS2, CITY, POST_CODE_ST_P)
                .selectCurrentAddress2(SELECT_VALUE)
                .addCommentInOrder(COMMENT)
                .checkboxLicenseAg(COMMENT)
                .checkOrderAndConfirm(NOTEBOOK);
        MakingAnOrderPage makingAnOrder = new MakingAnOrderPage();
        assertEquals(makingAnOrder.checkPurchaseOrder(), ORDER_CONFIRM);
    }


    @Severity(value = SeverityLevel.CRITICAL)
    @Test(description = "Проверить оформление покупки используя текущий адрес")
    public void makingOrderCurrentAddress() {
        MainPage mainPage = new MainPage();
        mainPage
                .lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        mainPage.transitionInBasket()
                .checkOrderInBasket(USE_SER_TEXT);
        mainPage.transitionInBasket()
                .transitionInMakingAnOrder()
                .useCurrentAddressMethod1(SELECT_VALUE)
                .selectCurrentAddress2(SELECT_VALUE)
                .addCommentInOrder(COMMENT)
                .checkboxLicenseAg(COMMENT)
                .confirmOrderMethod();
        MakingAnOrderPage makingAnOrder = new MakingAnOrderPage();
        assertEquals(makingAnOrder.checkPurchaseOrder(), ORDER_CONFIRM);
    }
}
