package com.voroj.opencart;

import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.voroj.opencart.Constants.*;
import static org.testng.Assert.assertEquals;

@Owner("Vorojeykin Dmitry")
@Epic("Smoke test")
public class LoginPageTest extends BaseTest {

    @Severity(value = SeverityLevel.BLOCKER)
    @Test(description = "Проверить авторизацию на сайте")
    public void loginTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkLogin()
                .enterEmail(EMAIL)
                .enterPassword(PASSWORD);
        LoginPage loginPage = new LoginPage();
        assertEquals(loginPage.checkTextLk(), TEXT_LK);
    }

    @DataProvider
    public Object[][] localData() {
        return new Object[][] {
                {EMAIL, INVALID_PASSWORD},
                {INVALID_EMAIL, PASSWORD},
                {EMPTY, EMPTY},
                {EMPTY, PASSWORD},
                {EMAIL, EMPTY},
                {INVALID_EMAIL, EMPTY},
                {EMPTY, INVALID_PASSWORD},
        };
    }

    @Severity(value = SeverityLevel.BLOCKER)
    @Test(dataProvider = "localData", description = "Проверить выполнение входа с различными значениями")
    public void providerTest(String emailVal, String passVal) {
        MainPage mainPage = new MainPage();
        mainPage.lkLogin()
                .enterEmail(emailVal)
                .enterPassword(passVal);
    }
}
