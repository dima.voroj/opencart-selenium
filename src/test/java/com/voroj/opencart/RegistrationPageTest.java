package com.voroj.opencart;

import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.voroj.opencart.Constants.*;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

@Owner("Vorojeykin Dmitry")
@Epic("Smoke test")
public class RegistrationPageTest extends BaseTest {

    @DataProvider
    public Object[][] registrationLocalData() {
        return new Object[][]{
                {INVALID_NAME, LASTNAME, EMAIL_REG, TELEPHONE, PASSWORD, PASSWORD},
                {NAME, INVALID_LASTNAME, EMAIL_REG, TELEPHONE, PASSWORD, PASSWORD},
                {NAME, LASTNAME, INVALID_EMAIL, TELEPHONE, PASSWORD, PASSWORD},
                {NAME, LASTNAME, EMAIL_REG, INVALID_TELEPHONE, PASSWORD, PASSWORD},
                {NAME, LASTNAME, EMAIL_REG, TELEPHONE, INVALID_PASSWORD, PASSWORD},
                {NAME, LASTNAME, EMAIL_REG, TELEPHONE, PASSWORD, INVALID_PASSWORD},
                {EMPTY,EMPTY,EMPTY,EMPTY,EMPTY,EMPTY},
        };
    }

    @Severity(value = SeverityLevel.BLOCKER)
    @Test(dataProvider = "registrationLocalData", description = "Проверить регистрацию нового пользователя" +
            " с неверными данными")
    public void negativRegistrationTest(String nameVal, String lastNameVal, String emailVal, String telepVal,
                                        String passVal, String passVal2) {
        MainPage mainPage = new MainPage();
        mainPage.lkRegistration()
                .submitRegData(nameVal, lastNameVal, emailVal, telepVal, passVal, passVal2);
    }

    @Severity(value = SeverityLevel.BLOCKER)
    @Test(description = "Проверить регистрацию нового пользователя")
    public void successfulRegistrationTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkRegistration()
                .submitRegData(NAME, LASTNAME, EMAIL_REG, TELEPHONE, PASSWORD, PASSWORD);
        PersonalAriaPage personalAria = new PersonalAriaPage();
        assertFalse(personalAria.checkRegistrationUser().contains(YES));
    }

    @Severity(value = SeverityLevel.BLOCKER)
    @Test(description = "Проверить регистрацию с повторными данными",
            dependsOnMethods = {"successfulRegistrationTest"}, alwaysRun = true)
    public void negativeRegistrationTest() {
        MainPage mainPage = new MainPage();
        mainPage.lkRegistration()
                .submitRegData(NAME, LASTNAME, EMAIL_REG, TELEPHONE, PASSWORD, PASSWORD);
        PersonalAriaPage personalAria = new PersonalAriaPage();
        assertEquals(personalAria.checkRegistrationUser(), NOT);
    }

}
