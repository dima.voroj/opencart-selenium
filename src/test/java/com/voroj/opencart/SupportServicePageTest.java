package com.voroj.opencart;

import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;

import static com.voroj.opencart.Constants.*;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Owner("Vorojeykin Dmitry")
@Epic("Smoke test")
public class SupportServicePageTest extends BaseTest {

    @Severity(value = SeverityLevel.CRITICAL )
    @Test(description = "Проверить адрес магазина и номер телефона сайта")
    public void checkContactsInfTest() {
        MainPage mainPage = new MainPage();
        mainPage.transitionInContacts();
        SupportServicePage supportService = new SupportServicePage();
        assertTrue(supportService.getAddress().contains(CHECK_ADDRESS));
        assertTrue(supportService.getContacts().contains(TEL_NUMBER_SITE));
    }

    @Severity(value = SeverityLevel.CRITICAL)
    @Test(description = "Проверить адрес другого магазина")
    public void checkAddressOurStoreTest() {
        MainPage mainPage = new MainPage();
        mainPage.transitionInContacts();
        SupportServicePage supportService = new SupportServicePage();
        assertTrue(supportService.getAddressAltufevo().contains(CHECK_ADDRESS2));

    }

    @Severity(value = SeverityLevel.CRITICAL)
    @Test(description = "Проверить телефонный номер другого магазина")
    public void checkNumberTelOurStoreTest() {
        MainPage mainPage = new MainPage();
        mainPage.transitionInContacts();
        SupportServicePage supportService = new SupportServicePage();
        assertTrue(supportService.getNumberStore().contains(TEL_NUMBER_SITE));

    }

    @Severity(value = SeverityLevel.BLOCKER)
    @Test(description = "Проверить отправку сообщения в службу поддержки")
    public void checkCreateAnApplicationTest() {
        MainPage mainPage = new MainPage();
        mainPage.transitionInContacts()
                .createAnApplication(NAME,EMAIL,MESSAGE_FIT_BACK);
        SupportServicePage supportService = new SupportServicePage();
        assertEquals(supportService.getTextContact(), CONTACTS);
    }

    @Severity(value = SeverityLevel.BLOCKER)
    @Test(description = "Проверить возврат товара не авторизованного пользователя")
    public void requestAnReturnOrderTest() {
        MainPage mainPage = new MainPage();
        mainPage.transitionInReturnOrder()
                .requestAReturn(NAME,LASTNAME,EMAIL,TELEPHONE,NUMBER_ORDER,DATE,
                                MARK_PRODUCT,NOTEBOOK,COUNT_ORDER,MESSAGE_FIT_BACK);
        SupportServicePage supportService = new SupportServicePage();
        assertEquals(supportService.getTextMessageOnReturn(), MESSAGE_RETURN);
    }


    @Severity(value = SeverityLevel.MINOR)
    @Test(description = "Проверить отображение карты сайта")
    public void checkMapSiteTest() {
        MainPage mainPage = new MainPage();
        mainPage.transitionInMapSite();
        SupportServicePage supportService = new SupportServicePage();
        assertEquals(supportService.getTextMapSite(), MAP_SITE_TEXT);
    }
}
