package com.voroj.opencart;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends BasePage {

    @FindBy(xpath = "//span[text()='Валюта']")
    private WebElement currency;

    @FindBy(xpath = "//span[text()='Язык']")
    private WebElement language;

    @FindBy(xpath = "//button[@name='en-gb']")
    private WebElement selectEnglish;

    @FindBy(xpath = "//*[@id='form-language']/div/button/span")
    private WebElement getTextLanguage;

    @FindBy(xpath = "//button[@name='ru-ru']")
    private WebElement selectRussian;

    @FindBy(xpath = "//button[@name='EUR']")
    private WebElement euro;

    @FindBy(xpath = "//button[@name='GBP']")
    private WebElement sterling;

    @FindBy(xpath = "//button[@name='USD']")
    private WebElement dollar;

    @FindBy(xpath = "//button[@name='RUB']")
    private WebElement rub;

    @FindBy(xpath = "//*[@id='form-currency']/div/button/strong")
    private WebElement getIconCurrent;

    @FindBy(xpath = "//a[@title='Личный кабинет']")
    private WebElement lk;

    @FindBy(xpath = "//*[@id='top-links']/ul/li[2]/ul/li[1]/a")
    private WebElement lkReg;

    @FindBy(xpath = "//*[@id='top-links']/ul/li[2]/ul/li[2]/a")
    private WebElement logIn;

    @FindBy(xpath = "//span[text()='123456789']")
    private WebElement numberTelephoneSite;

    @FindBy(xpath = "//li/a[contains(text(),'Desktops')]")
    private WebElement desktop;

    @FindBy(xpath = "//div/a[contains(text(),'Desktops')]")
    private WebElement showAllDesktops;

    @FindBy(xpath = "//a[@title='Корзина']")
    private WebElement mainPageBasket;

    @FindBy(xpath = "//div/a[contains(text(),'Laptops & Notebooks (5)')]")
    private WebElement showNotebooks;

    @FindBy(xpath = "//*[@id='content']/div[4]/div[1]/div/div[2]/div[2]/button[1]")
    private WebElement showHpNotebook;

    @FindBy(xpath = "//a[text()='Контакты']")
    private WebElement contacts;

    @FindBy(xpath = "//a[text()='Возврат товара']")
    private WebElement returnOrder;

    @FindBy(xpath = "//a[text()='Карта сайта']")
    private WebElement mapSite;

    @FindBy(xpath = "//a[text()='Производители']")
    private WebElement manufactures;

    @FindBy(xpath = "//a[text()='Подарочные сертификаты']")
    private WebElement giftCertificate;

    @FindBy(xpath = "//a[text()='Партнерская программа']")
    private WebElement newPartner;

    @FindBy(xpath = "//a[text()='Акции']")
    private WebElement stock;

    public AdditionallyPage transitionInStock() {
        stock.click();
        return new AdditionallyPage();
    }

    public AdditionallyPage transitionInRegistrationNewPartner() {
        newPartner.click();
        return new AdditionallyPage();
    }

    public void transitionInManufacturers() {
        manufactures.click();
        new AdditionallyPage();
    }

    public AdditionallyPage transitionInGiftCertificate() {
        giftCertificate.click();
        return new AdditionallyPage();
    }

    public SupportServicePage transitionInContacts() {
        contacts.click();
        return new SupportServicePage();
    }

    public SupportServicePage transitionInReturnOrder() {
        returnOrder.click();
        return new SupportServicePage();
    }

    public void transitionInMapSite() {
        mapSite.click();
        new SupportServicePage();
    }

    public BasketPage transitionInBasket() {
        mainPageBasket.click();
        return new BasketPage();
    }

    public PurchasesPage transitionAnPurchases() {
        desktop.click();
        showAllDesktops.click();
        return new PurchasesPage();
    }

    public String checkNumber() {
        return numberTelephoneSite.getText();
    }


    public LoginPage lkLogin() {
        lk.click();
        logIn.click();
        return new LoginPage();
    }

    public RegistrationPage lkRegistration() {
        lk.click();
        lkReg.click();
        return new RegistrationPage();
    }

    public void setSelectEnglish() {
        language.click();
        selectEnglish.click();
    }

    public String getTexAllLanguage() {
       return getTextLanguage.getText();
    }

    public void setSelectRussian() {
        language.click();
        selectRussian.click();
    }


    public void setEuro() {
        currency.click();
        euro.click();
    }

    public String getTextCurrent() {
        return getIconCurrent.getText();
    }

    public void setSterling() {
        currency.click();
        sterling.click();
    }

    public void setDollar() {
        currency.click();
        dollar.click();
    }

    public void setRub() {
        currency.click();
        rub.click();
    }


    public MainPage() {
        driver.get("https://demo-opencart.ru/");
        PageFactory.initElements(driver, this);
    }
}
