package com.voroj.opencart;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PersonalAriaPage extends BasePage {

    public PersonalAriaPage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//a[text()='Личный кабинет']")
    private WebElement returnInLk;

    @FindBy(xpath = "//a[text()='История заказов']")
    private WebElement orderHistory;

    @FindBy(xpath = "//a[text()='Транзакции']")
    private WebElement transaction;

    @FindBy(xpath = "//a[text()='Загрузки']")
    private WebElement loads;

    @FindBy(xpath = "//div[@id='content']/p[1]")
    private WebElement congratulationsNewUser;

    @FindBy(xpath = "//*[@id='account-register']/div[1]")
    private WebElement thisEmailInUse;

    @FindBy(xpath = "//*[@id='content']/div[1]/table/tbody/tr[1]/td[7]/a")
    private WebElement checkInfOrder;

    @FindBy(xpath = "//*[@id='content']/div[1]/table/tbody/tr/td[2]")
    private WebElement checkModelOrder;

    @FindBy(xpath = "//a[@title='Личный кабинет']")
    private WebElement lk;

    @FindBy(xpath = "//a[@data-original-title='Вернуть']")
    private WebElement returnOrder;

    @FindBy(xpath = "//*[@id='content']/form/fieldset[2]/div[4]/div/div[3]/label/input")
    private WebElement reasonOfReturn;

    @FindBy(xpath = "//textarea[@id='input-comment']")
    private WebElement descriptionProblemsOrder;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement submit;

    @FindBy(xpath = "//*[@id='content']/p[1]")
    private WebElement getText;

    @FindBy(xpath = "//*[@id='account-transaction']/ul/li[3]/a")
    private WebElement getTextHistoryTransactions;

    @FindBy(xpath = "//div[@id='content']/h1")
    private WebElement getTextPersonalLk;

    @FindBy(xpath = "//a[text()='Файлы для скачивания']")
    private WebElement downloadsFile;

    @FindBy(xpath = "//a[text()='Выход']")
    private WebElement logOut;

    @FindBy(xpath = "//div[@id='content']/p[text()='Вы вышли из Личного Кабинета.']")
    private WebElement logoutText;

    public String logoutTextMethod() {
        return logoutText.getText();
    }

    public void logOutMethod() {
        lk.click();
        logOut.click();
    }

    public String getTextDownloadsFileMethod() {
        return downloadsFile.getText();
    }

    public PersonalAriaPage checkLoadsMethod() {
        lk.click();
        loads.click();
        return this;
    }

    public String getTextHistTransMethod() {
        return getTextHistoryTransactions.getText();
    }

    public PersonalAriaPage checkTransactionMethod() {
        lk.click();
        transaction.click();
        return this;
    }

    public MainPage checkReturnLk() {
        returnInLk.click();
        return new MainPage();
    }

    public String checkRegistrationUser() {
        String yes = congratulationsNewUser.getText();
        String not = thisEmailInUse.getText();
        return congratulationsNewUser.isDisplayed() ? yes : not;
    }

    public PersonalAriaPage checkHistoryOrders() {
        lk.click();
        orderHistory.click();
        return this;
    }

    public PersonalAriaPage checkInfOrderMethod() {
        checkInfOrder.click();
        return this;
    }

    public String checkModelOrderMethod() {
        return checkModelOrder.getText();
    }

    public PersonalAriaPage returnOrderMethod() {
        returnOrder.click();
        return this;
    }

    public PersonalAriaPage returnOrderErrorMethod() {
        reasonOfReturn.click();
        return this;
    }

    public PersonalAriaPage descriptionProblemsOrderMethod(String text) {
        descriptionProblemsOrder.click();
        descriptionProblemsOrder.sendKeys(text);
        return this;
    }

    public void submitMethod() {
        submit.click();
    }

    public String getTextMethod() {
        return getText.getText();
    }

}
