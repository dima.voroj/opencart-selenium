package com.voroj.opencart;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BasketPage extends BasePage {

    public BasketPage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//li/a[contains(text(),'Desktops')]")
    private WebElement desktop;

    @FindBy(xpath = "//div/a[contains(text(),'Desktops')]")
    private WebElement showAllDesktops;

    @FindBy(xpath = "//div/a[contains(text(),'Laptops & Notebooks (5)')]")
    private WebElement showNotebooks;

    @FindBy(xpath = "//*[@id='content']/div[4]/div[1]/div/div[2]/div[2]/button[1]")
    private WebElement showHpNotebook;

    @FindBy(xpath = "//*[@id='button-cart']")
    private WebElement purchaseNotebook;

    @FindBy(xpath = "//input[@id='input-option225']")
    private WebElement dateDelivery;

    @FindBy(xpath = "//li/a[@title='Корзина']")
    private WebElement checkBasket;

    @FindBy(xpath = "//*[@id='content']/div[3]/div[2]/a")
    private WebElement orderRing;

    @FindBy(xpath = "//*[@id='content']/div[3]/div[1]/a")
    private WebElement continueShopping;

    @FindBy(xpath = "//*[@id='accordion']/div[1]/div[1]/h4/a")
    private WebElement useCoupon;

    @FindBy(xpath = "//*[@id='input-coupon']")
    private WebElement enterCodeCoupon;

    @FindBy(xpath = "//input[@id='button-coupon']")
    private WebElement applyCoupon;

    @FindBy(xpath = "//*[@id='checkout-cart']/div[1]/text()")
    private WebElement checkTextDisplayedUseCoupon;

    @FindBy(xpath = "//*[@id='accordion']/div[2]/div[1]/h4/a")
    private WebElement calkDelivery;

    @FindBy(xpath = "//select[@id='input-country']")
    private WebElement inputCountry;

    @FindBy(xpath = "//select[@id='input-country']/option[@value=176]")
    private WebElement inputCountryRF;

    @FindBy(xpath = "//select[@id='input-zone']")
    private WebElement inputZone;

    @FindBy(xpath = "//select[@id='input-zone']/option[@value=2735]")
    private WebElement inputZoneLO;

    @FindBy(xpath = "//input[@name='postcode']")
    private WebElement postCode;

    @FindBy(xpath = "//*[@id='button-quote']")
    private WebElement checkPriceDelivery;

    @FindBy(xpath = "//*[@id='content']/div[2]/div/table/tbody/tr[2]")
    private WebElement priceDelivery;

    @FindBy(xpath = "//*[@id='modal-shipping']/div/div/div[3]/button")
    private WebElement notDelivery;

    @FindBy(xpath = "//*[@id='modal-shipping']/div/div/div[2]/div/label/input")
    private WebElement checkboxPriceDelivery;

    @FindBy(xpath = "//input[@value='Применить Доставку']")
    private WebElement applyDelivery;

    @FindBy(xpath = "//*[@id='accordion']/div[3]/div[1]/h4/a")
    private WebElement useGiftCertificate;

    @FindBy(xpath = "//input[@id='input-voucher']")
    private WebElement inputCodeGift;

    @FindBy(xpath = "//*[@id='button-voucher']")
    private WebElement applyGiftCertificate;

    @FindBy(xpath = "//*[@id='content']/form/div/table/tbody/tr/td[4]/div/input")
    private WebElement countOrderBasket;

    @FindBy(xpath = "//input[@id='input-quantity']")
    private WebElement countOrder;

    @FindBy(xpath = "//button[@data-original-title='Обновить']")
    private WebElement updateCountOrder;

    @FindBy(xpath = "//div[@id='content']/h1")
    private WebElement checkAddOrder;

    @FindBy(xpath = "//*[@id='checkout-cart']/div[1]")
    private WebElement checkElDisplayed;

    public Boolean checkElDisplayedMethod() {
        return checkElDisplayed.isDisplayed();
    }


    public MakingAnOrderPage transitionInMakingAnOrder() {
        orderRing.click();
        return new MakingAnOrderPage();
    }

    public void checkOrderInBasket(String text) {
        if (!getTextCheckModelOrder().contains(text)) {
            showAddNotebookInBasket("2022-07-30", "1");
        } else {
            checkBasket.click();
            countOrderMethod2("1");
        }
    }

    public String getTextCheckModelOrder() {
        checkBasket.click();
        return checkAddOrder.getText();
    }

    public void countOrderMethod2(String count) {
        countOrderBasket.click();
        countOrderBasket.clear();
        countOrderBasket.sendKeys(count);
        updateCountOrder.click();
    }

    public String countOrderMethod(String count) {
        countOrder.click();
        countOrder.clear();
        countOrder.sendKeys(count);
        return count;
    }

    public void showAddNotebookInBasket(String date, String count) {
        desktop.click();
        showAllDesktops.click();
        showNotebooks.click();
        showHpNotebook.click();
        setDateDelivery(date);
        setCountOrders(count);
        purchaseNotebook.click();
    }

    public void setDateDelivery(String date) {
        dateDelivery.click();
        dateDelivery.clear();
        dateDelivery.sendKeys(date);
    }

    public void setCountOrders(String count) {
        countOrder.click();
        countOrder.clear();
        countOrder.sendKeys(count);
    }

    public void addCoupon(String coupon) {
        useCoupon.click();
        enterCodeCoupon.sendKeys(coupon);
        applyCoupon.click();
    }

    public void calkPriceDelivery(String indexPost) {
        calkDelivery.click();
        inputCountry.click();
        inputCountryRF.click();
        inputZone.click();
        inputZoneLO.click();
        postCode.sendKeys(indexPost);
        checkPriceDelivery.click();
        checkboxPriceDelivery.click();
        applyDelivery.click();
    }

    public String getPriceDelivery() {
        return priceDelivery.getText();
    }

    public void useGiftCertificateMethod(String certificate) {
        useGiftCertificate.click();
        inputCodeGift.sendKeys(certificate);
        applyGiftCertificate.click();
    }

    public void orderRingEnter() {
        orderRing.click();
    }

    public MainPage continueShoppingMethod() {
        continueShopping.click();
        return new MainPage();
    }
}
