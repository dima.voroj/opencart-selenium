package com.voroj.opencart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class MakingAnOrderPage extends BasePage {

    public MakingAnOrderPage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id='collapse-payment-address']/div/form/div[1]/label/input")
    private WebElement useCurrentDelivery;

    @FindBy(xpath = "//*[@id='collapse-payment-address']/div/form/div[3]/label/input")
    private WebElement newAddressDelivery;

    @FindBy(xpath = "//input[@name='firstname']")
    private WebElement name;

    @FindBy(xpath = "//input[@name='lastname']")
    private WebElement lastName;

    @FindBy(xpath = "//input[@name='company']")
    private WebElement company;

    @FindBy(xpath = "//input[@name='address_1']")
    private WebElement address1;

    @FindBy(xpath = "//input[@name='address_2']")
    private WebElement address2;

    @FindBy(xpath = "//input[@name='city']")
    private WebElement city;

    @FindBy(xpath = "//input[@name='postcode']")
    private WebElement postcode;

    @FindBy(xpath = "//select[@name='country_id']")
    private WebElement selectCountry;

    @FindBy(xpath = "//select[@name='country_id']/option[@value=176]")
    private WebElement selectRF;

    @FindBy(xpath = "//select[@id='input-payment-zone']")
    private WebElement selectRegion;

    @FindBy(xpath = "//select[@name='zone_id']/option[@value=2735]")
    private WebElement selectLO;

    @FindBy(xpath = "//input[@value='Продолжить']")
    private WebElement next1;

    @FindBy(xpath = "//*[@id='button-shipping-address']")
    private WebElement next2;

    @FindBy(xpath = "//*[@id='button-payment-method']")
    private WebElement next3;

    @FindBy(xpath = "//*[@id='button-shipping-method']")
    private WebElement next4;

    @FindBy(xpath = "//*[@id='collapse-shipping-method']/div/p[4]/textarea")
    private WebElement commit;

    @FindBy(xpath = "//*[@id='collapse-payment-method']/div/p[3]/textarea")
    private WebElement commit2;

    @FindBy(xpath = "//input[@name='agree']")
    private WebElement checkboxLicenseAgr;

    @FindBy(xpath = "//*[@id='button-confirm']")
    private WebElement confirmOrderButton;

    @FindBy(xpath = "//*[@id='content']/p[1]")
    private WebElement message;

    @FindBy(xpath = "//*[@id='payment-existing']/select")
    private WebElement dropDownAddress1;

    @FindBy(xpath = "//*[@id='shipping-existing']/select")
    private WebElement dropDownAddress2;

    @FindBy(xpath = "//*[@id='shipping-existing']/select/option[@value='303']")
    private WebElement selectUseAddress;


    public String checkPurchaseOrder() {
        return message.getText();
    }

    public void checkOrderAndConfirm(String orderName) {
        WebElement element = driver
                .findElement(By.xpath("//*[@id=\"collapse-checkout-confirm\"]/div/div[1]/table/tbody/tr/td[1]"));
        if (element.getText().contains(orderName)) {
            confirmOrderButton.click();
        }
    }

    public void confirmOrderMethod() {
        confirmOrderButton.click();
    }


    public MakingAnOrderPage checkboxLicenseAg(String commitValue) {
        commit2.click();
        commit2.sendKeys(commitValue);
        checkboxLicenseAgr.click();
        next3.click();
        return this;
    }


    public MakingAnOrderPage addCommentInOrder(String commitValue) {
        commit.click();
        commit.sendKeys(commitValue);
        next4.click();
        return this;
    }


    public MakingAnOrderPage selectCurrentAddress2(String value) {
        Select select = new Select(dropDownAddress2);
        select.selectByValue(value);
        next2.click();
        return this;
    }


    public MakingAnOrderPage useNewAddressMethod(String nameValue, String lastNameValue, String companyValue, String address1Value,
                                                 String address2Value, String cityValue, String postCodValue) {
        newAddressDelivery.click();
        name.sendKeys(nameValue);
        lastName.sendKeys(lastNameValue);
        company.sendKeys(companyValue);
        address1.sendKeys(address1Value);
        address2.sendKeys(address2Value);
        city.sendKeys(cityValue);
        postcode.sendKeys(postCodValue);
        selectCountry.click();
        selectRF.click();
        selectRegion.click();
        selectLO.click();
        next1.click();
        return this;
    }

    public MakingAnOrderPage useCurrentAddressMethod1(String value) {
        Select select = new Select(dropDownAddress1);
        select.selectByValue(value);
        next1.click();
        return this;
    }
}
