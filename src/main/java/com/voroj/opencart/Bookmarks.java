package com.voroj.opencart;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Bookmarks extends BasePage {

    public Bookmarks() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//button[@data-original-title='В закладки']")
    private WebElement addBookmarks;

    @FindBy(xpath = "//div[@class='alert alert-success alert-dismissible']")
    private WebElement textAddOrder;

    @FindBy(xpath = "//span[@class='hidden-xs hidden-sm hidden-md'][text()='Закладки (1)']")
    private WebElement checkBookmarks;

    @FindBy(xpath = "//a[@data-original-title='Удалить']")
    private WebElement clearBookmarks;

    @FindBy(xpath = "//div/p[text()='Ваша корзина пуста!']")
    private WebElement textClearBookmarks;

    public Bookmarks checkBookmarksMethod() {
        checkBookmarks.click();
        return this;
    }

    public void clearBookmarksMethod() {
        clearBookmarks.click();
    }

    public String getTextClearBookmarks() {
        return textClearBookmarks.getText();
    }

    public String getTextCheckBookmarks() {
        return checkBookmarks.getText();
    }

    public void addBookMarksMethod() {
        addBookmarks.click();
    }

    public String getTextAddOrder() {
        return textAddOrder.getText();
    }
}
