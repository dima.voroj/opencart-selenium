package com.voroj.opencart;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {

    @FindBy(id = "input-email")
    private WebElement inputEmail;

    @FindBy(id = "input-password")
    private WebElement inputPassword;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement submit;

    @FindBy(xpath = "//li[@class='dropdown']/a[@title='Личный кабинет']")
    private WebElement privateAria;

    @FindBy(xpath = "//*[@id='content']/h2[1]")
    private WebElement textLk;

    public LoginPage() {
        PageFactory.initElements(driver, this);
    }

    public LoginPage enterEmail(String ema) {
        inputEmail.sendKeys(ema);
        return this;
    }

    public void enterPassword(String pas) {
        inputPassword.sendKeys(pas, Keys.ENTER);
        new PersonalAriaPage();
    }

    public String checkTextLk() {
        return textLk.getText();
    }
}
