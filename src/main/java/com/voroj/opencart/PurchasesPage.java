package com.voroj.opencart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class PurchasesPage extends BasePage {

    public PurchasesPage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div/a[contains(text(),'Laptops & Notebooks (5)')]")
    private WebElement showNotebooks;

    @FindBy(xpath = "//*[@id='content']/div[4]/div[1]/div/div[2]/div[2]/button[1]")
    private WebElement showHpNotebook;

    @FindBy(xpath = "//input[@id='input-option225']")
    private WebElement dateDelivery;

    @FindBy(xpath = "//input[@id='input-quantity']")
    private WebElement countOrder;

    @FindBy(xpath = "//*[@id='button-cart']")
    private WebElement purchaseNotebook;

    @FindBy(xpath = "//div/a[text()='HP LP3065']")
    private WebElement markProduct;

    @FindBy(xpath = "//input[@name='quantity[7346]']")
    private WebElement countOrderInBasket;

    @FindBy(xpath = "//li/a[contains(text(),'Desktops')]")
    private WebElement desktop;

    @FindBy(xpath = "//div/a[contains(text(),'Desktops')]")
    private WebElement showAllDesktops;

    public String checkAddOrderInBasket() {
        List<WebElement> list = driver.findElements(By.xpath("//*[@id='product-product']/div[1]"));
        StringBuilder str = new StringBuilder();
        for (WebElement element : list) {
            str.append(element.getText());
        }
        return str.toString();
    }

    public void setDateDelivery(String date) {
        dateDelivery.click();
        dateDelivery.clear();
        dateDelivery.sendKeys(date);
    }

    public void setCountOrders(String count) {
        countOrder.click();
        countOrder.clear();
        countOrder.sendKeys(count);
    }

    public void showAddNotebookInBasket(String date, String count) {
//        desktop.click();
//        showAllDesktops.click();
        showNotebooks.click();
        showHpNotebook.click();
        setDateDelivery(date);
        setCountOrders(count);
        purchaseNotebook.click();
    }

    public Bookmarks addBookmarksMet() {
        showNotebooks.click();
        showHpNotebook.click();
        return new Bookmarks();
    }


}
