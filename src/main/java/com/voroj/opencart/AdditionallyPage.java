package com.voroj.opencart;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdditionallyPage extends BasePage {
    public AdditionallyPage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//a[text()='Apple']")
    private WebElement apple;

    @FindBy(xpath = "//a[text()='Canon']")
    private WebElement canon;

    @FindBy(xpath = "//a[text()='Hewlett-Packard']")
    private WebElement hewlettPackard;

    @FindBy(xpath = "//a[text()='Palm']")
    private WebElement palm;

    @FindBy(xpath = "//a[text()='Sony']")
    private WebElement sony;

    @FindBy(xpath = "//a[text()='HTC']")
    private WebElement htc;

    @FindBy(xpath = "//input[@id='input-to-name']")
    private WebElement receiverName;

    @FindBy(xpath = "//input[@id='input-to-email']")
    private WebElement receiverEmail;

    @FindBy(xpath = "//input[@id='input-from-name']")
    private WebElement youName;

    @FindBy(xpath = "//input[@id='input-from-email']")
    private WebElement youEmail;

    @FindBy(xpath = "//input[@type='radio'][@value='7']")
    private WebElement birthday;

    @FindBy(xpath = "//input[@type='radio'][@value='6']")
    private WebElement christmas;

    @FindBy(xpath = "//input[@type='radio'][@value='8']")
    private WebElement general;

    @FindBy(xpath = "//textarea[@name='message']")
    private WebElement message;

    @FindBy(xpath = "//input[@name='amount']")
    private WebElement amount;

    @FindBy(xpath = "//input[@type='checkbox']")
    private WebElement checkbox;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement submit;

    @FindBy(xpath = "//div[@id='content']/p")
    private WebElement messageThankYou;

    @FindBy(xpath = "//a[text()='Продолжить']")
    private WebElement continueNewPartnerEnter;

    @FindBy(xpath = "//input[@id='input-firstname']")
    private WebElement namePatronymig;

    @FindBy(xpath = "//input[@id='input-lastname']")
    private WebElement surname;

    @FindBy(xpath = "//input[@id='input-email']")
    private WebElement email;

    @FindBy(xpath = "//input[@id='input-telephone']")
    private WebElement telephone;

    @FindBy(xpath = "//input[@id='input-company']")
    private WebElement company;

    @FindBy(xpath = "//input[@id='input-website']")
    private WebElement website;

    @FindBy(xpath = "//input[@id='input-tax']")
    private WebElement inn;

    @FindBy(xpath = "//input[@value='bank']")
    private WebElement checkboxBank;

    @FindBy(xpath = "//input[@id='input-bank-name']")
    private WebElement nameBank;

    @FindBy(xpath = "//input[@id='input-bank-branch-number']")
    private WebElement branchNumber;

    @FindBy(xpath = "//input[@id='input-bank-swift-code']")
    private WebElement bik;

    @FindBy(xpath = "//input[@id='input-bank-account-name']")
    private WebElement corporateAccount;

    @FindBy(xpath = "//input[@id='input-bank-account-number']")
    private WebElement checkingAccount;

    @FindBy(xpath = "//input[@value='paypal']")
    private WebElement checkboxPayPal;

    @FindBy(xpath = "//input[@id='input-paypal']")
    private WebElement inputPayPal;

    @FindBy(xpath = "//input[@value='cheque']")
    private WebElement checkboxCheque;

    @FindBy(xpath = "//input[@id='input-cheque']")
    private WebElement inputCheque;

    @FindBy(xpath = "//input[@id='input-password']")
    private WebElement password;

    @FindBy(xpath = "//input[@id='input-confirm']")
    private WebElement confirmPassword;

    @FindBy(xpath = "//input[@name='agree']")
    private WebElement checkboxAgree;

    @FindBy(xpath = "//div[@id='content']/p")
    private WebElement textPartnerAcc;

    @FindBy(xpath = "//div[@id='content']/h2")
    private WebElement textLk;

    @FindBy(xpath = "//div[@id='content']/h2")
    private WebElement textStock;

    public String getTextStock() {
        return textStock.getText();
    }

    public String getTextLk() {
        return textLk.getText();
    }

    public String getTextPartnerAcc() {
        return textPartnerAcc.getText();
    }

    public void signIn(String emailVal, String passVal) {
        email.sendKeys(emailVal);
        password.sendKeys(passVal);
        submit.click();
    }

    public void registrationPartnerAcc(String namePatromigVal, String lastNameVal, String emailVal,
                                       String telephoneVal, String companyVal, String websiteVal,
                                       String innVal, String passwordVal, String passConfirmVal) {
        continueNewPartnerEnter.click();
        namePatronymig.sendKeys(namePatromigVal);
        surname.sendKeys(lastNameVal);
        email.sendKeys(emailVal);
        telephone.sendKeys(telephoneVal);
        company.sendKeys(companyVal);
        website.sendKeys(websiteVal);
        inn.sendKeys(innVal);
        selectCheque("Sacha");
        password.sendKeys(passwordVal);
        confirmPassword.sendKeys(passConfirmVal);
        checkboxAgree.click();
        submit.click();
    }


    public void selectCheque(String nameCheque) {
        checkboxCheque.click();
        inputCheque.sendKeys(nameCheque);
    }

    public void selectBank(String nameBankVal, String branchNumberVal, String bikVal,
                                   String corporateAkVal, String checkingAkVal) {
        checkboxBank.click();
        nameBank.sendKeys(nameBankVal);
        branchNumber.sendKeys(branchNumberVal);
        bik.sendKeys(bikVal);
        corporateAccount.sendKeys(corporateAkVal);
        checkingAccount.sendKeys(checkingAkVal);
    }

    public void selectPayPal(String payPalEmail) {
        checkboxPayPal.click();
        inputPayPal.sendKeys(payPalEmail);
    }

    public String getTextMessageThank() {
        return messageThankYou.getText();
    }

    public void inputData(String receiverNameValue, String receiverEmailVal, String youNameVal,
                          String youEmailVal, String messageVal, String amountVal) {
        receiverName.sendKeys(receiverNameValue);
        receiverEmail.sendKeys(receiverEmailVal);
        youName.clear();
        youName.sendKeys(youNameVal);
        youEmail.clear();
        youEmail.sendKeys(youEmailVal);
        birthday.click();
        message.sendKeys(messageVal);
        amount.clear();
        amount.sendKeys(amountVal);
        checkbox.click();
        submit.click();
    }

    public String checkApple() {
        return apple.getText();
    }

    public String checkCanon() {
        return canon.getText();
    }

    public String checkHewlett() {
        return hewlettPackard.getText();
    }

    public String checkPalm() {
        return palm.getText();
    }

    public String checkSony() {
        return sony.getText();
    }

    public String checkHtc() {
        return htc.getText();
    }
}
