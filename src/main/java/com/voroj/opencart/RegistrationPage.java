package com.voroj.opencart;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegistrationPage extends BasePage {

    public RegistrationPage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "input-firstname")
    private WebElement firstName;

    @FindBy(id = "input-lastname")
    private WebElement lastName;

    @FindBy(id = "input-email")
    private WebElement email;

    @FindBy(id = "input-telephone")
    private WebElement telephone;

    @FindBy(id = "input-password")
    private WebElement password;

    @FindBy(id = "input-confirm")
    private WebElement passwordConfirm;

    @FindBy(xpath = "//input[@type='checkbox']")
    private WebElement agree;

    @FindBy(xpath = "//label[@class='radio-inline'][1]")
    private WebElement mailingListYes;

    @FindBy(xpath = "//label[@class='radio-inline'][2]")
    private WebElement mailingListNot;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement submit;

    public void enterFirstNameReg(String firstnameValue) {
        firstName.sendKeys(firstnameValue);
    }

    public void enterLastNameReg(String lastnameValue) {
        lastName.sendKeys(lastnameValue);
    }

    public void enterEmailReg(String emailValue) {
        email.sendKeys(emailValue);
    }

    public void enterTelReg(String telValue) {
        telephone.sendKeys(telValue);
    }

    public void enterPassReg(String passValue) {
        password.sendKeys(passValue);
    }

    public void enterPassConfirmReg(String passConfValue) {
        passwordConfirm.sendKeys(passConfValue);
    }

    public void newsSubscription() {
        mailingListYes.click();
    }

    public void enterCheckboxLicense() {
        agree.click();
    }

    public PersonalAriaPage enterSubmit() {
        submit.click();
        return new PersonalAriaPage();
    }

    public PersonalAriaPage submitRegData(String nameValue, String lastnameValue, String emailValue,
                                          String telephoneValue, String passValue, String pass2Value) {
        enterFirstNameReg(nameValue);
        enterLastNameReg(lastnameValue);
        enterEmailReg(emailValue);
        enterTelReg(telephoneValue);
        enterPassReg(passValue);
        enterPassConfirmReg(pass2Value);
        newsSubscription();
        enterCheckboxLicense();
        return enterSubmit();
    }
}
