package com.voroj.opencart;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SupportServicePage extends BasePage {

    public SupportServicePage() {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//address")
    private WebElement checkAddress;

    @FindBy(xpath = "//*[@id='content']/div[1]/div/div/div[2]")
    private WebElement checkNumber;

    @FindBy(xpath = "//*[@id='accordion']/div/div[1]/h4/a")
    private WebElement ourStores;

    @FindBy(xpath = "//*[@id='collapse-location2']/div/div/div[1]")
    private WebElement checkAddress2;

    @FindBy(xpath = "//*[@id='collapse-location2']/div/div/div[2]")
    private WebElement checkNumber2;

    @FindBy(xpath = "//input[@id='input-name']")
    private WebElement name;

    @FindBy(xpath = "//input[@id='input-email']")
    private WebElement email;

    @FindBy(xpath = "//textarea[@id='input-enquiry']")
    private WebElement message;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement submit;

    @FindBy(xpath = "//div[@id='content']/h1")
    private WebElement contactText;

    @FindBy(xpath = "//input[@id='input-firstname']")
    private WebElement returnOrderName;

    @FindBy(xpath = "//input[@id='input-lastname']")
    private WebElement returnOrderLastName;

    @FindBy(xpath = "//input[@id='input-email']")
    private WebElement returnOrderEmail;

    @FindBy(xpath = "//input[@id='input-telephone']")
    private WebElement returnOrderTelephone;

    @FindBy(xpath = "//input[@id='input-order-id']")
    private WebElement numberOrder;

    @FindBy(xpath = "//input[@id='input-date-ordered']")
    private WebElement dateOrder;

    @FindBy(xpath = "//input[@id='input-product']")
    private WebElement nameProduct;

    @FindBy(xpath = "//input[@id='input-model']")
    private WebElement model;

    @FindBy(xpath = "//input[@id='input-quantity']")
    private WebElement countOrder;

    @FindBy(xpath = "//input[@value='3']")
    private WebElement reasonForReturn;

    @FindBy(xpath = "//input[@name='opened'][@value='0']")
    private WebElement orderUnpackedNot;

    @FindBy(xpath = "//input[@name='opened'][@value='1']")
    private WebElement orderUnpackedYes;

    @FindBy(xpath = "//textarea[@id='input-comment']")
    private WebElement descriptionsDefect;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement submitReturn;

    @FindBy(xpath = "//div[@id='content']/p")
    private WebElement messageOnReturn;

    @FindBy(xpath = "//div[@id='content']/h1")
    private WebElement mapSite;


    public String getTextMapSite() {
        return mapSite.getText();
    }

    public String getTextMessageOnReturn() {
        return messageOnReturn.getText();
    }


    public void requestAReturn(String nameVal, String lastNmeVal, String emailVal,
                               String telVal, String numberOrderVal, String dateOrderVal,
                               String nameOrderVal, String modelVal, String countOrderVal,
                               String descriptionVal) {
        returnOrderName.sendKeys(nameVal);
        returnOrderLastName.sendKeys(lastNmeVal);
        returnOrderEmail.sendKeys(emailVal);
        returnOrderTelephone.sendKeys(telVal);
        numberOrder.clear();
        numberOrder.sendKeys(numberOrderVal);
        dateOrder.sendKeys(dateOrderVal);
        nameProduct.sendKeys(nameOrderVal);
        model.sendKeys(modelVal);
        countOrder.sendKeys(countOrderVal);
        reasonForReturn.click();
        orderUnpackedNot.click();
        descriptionsDefect.sendKeys(dateOrderVal);
        submitReturn.click();
    }

    public String getTextContact() {
        return contactText.getText();
    }

    public void createAnApplication(String nameValue, String emailValue, String messageValue) {
        name.sendKeys(nameValue);
        email.sendKeys(emailValue);
        message.sendKeys(messageValue);
        submit.click();
    }

    public String getNumberStore() {
        ourStores.click();
        return checkNumber2.getText();
    }

    public String getAddressAltufevo() {
        ourStores.click();
        return checkAddress2.getText();
    }

    public String getAddress() {
        return checkAddress.getText();
    }

    public String getContacts() {
        return checkNumber.getText();
    }

}
